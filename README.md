# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version: 1.0.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

* Configuration
	1. Goto the mapped directory/folder then run "AmazingRace.sln" using Visual Studio 2017 in Administrator mode.
	2. Right click "Solution 'AmazingRace'" then click "Restore NuGet Packages"
	3. Once the packages have been restored, right click "Solution 'AmazingRace'" then click "Properties".
	4. Ensure that it has a "Multiple startup projects" having the startup of the following then click "Apply" then click "OK":
		1. AmazingRace.Web.Application
		2. Services.Host.IIS
		
* Dependencies
	1. Microsoft Visual Studio 2017
	2. SQL Server Management Studio 2014
	
* Database configuration
	1. Open your local SQL Server 2014 Management Studio in Administrator mode
	2. Connect to "localhost".
	3. Right click "Databases" then click "Attach".
	4. Click "Add" then browse to the mapped directory/folder above then goto BE.
	5. Click "AmazingRace.mdf" then click "OK" then click "OK"
	
* How to run tests
	1. Testing the Web Application
		1. Once set-up has been completed, right click "Solution" then click "Clean solution".
		2. Right click "Solution" then click "Build" or "Rebuild" solution.
		3. Since we're ready to run the application, click "Start" on the menu to run the application.
		4. The application then runs both "AmazingRace.Web.Application" and "Services.Host.IIS" applications.
		5. Once both applications have been loaded, you may now use the application.
			1. If you don't have an account yet, kindly register your details then click "Create Account"
			2. If you already have an account, you may now continue to use the application by logging in on the upper-left part of the page.
			3. You may use the accounts below
				* Email: charlesyu@yahoo.com
				* Password: 12345678
	2. Running the unit test of the web application
		1. Click "Test" menu then under "Windows" sub menu, click "Test Explorer".
		2. Rebuild the solution by following step 1. > 1. under How to run tests.
		3. Click "Run All" under the Test Explorer window to run all unit tests.
		
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact