﻿using AmazingRace.Profile.DataContext;
using AmazingRace.Profile.Model.Entities;
using AmazingRace.Race.DataContext;
using AmazingRace.Race.Model.Entities;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Unity;

namespace Services.Host.IIS
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            RegisterExtension.Register();
        }

        protected void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            Response.AppendHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            Response.AppendHeader("X-Xss-Protection", "1; mode=block");
            if (!Request.RawUrl.Contains(".gif") && !Request.RawUrl.Contains(".png"))
            {
                Response.AppendHeader("X-Content-Type-Options", "nosniff");
            }
            Response.AppendHeader("X-Frame-Options", "SAMEORIGIN");
        }

        #region Methods...
        /// <summary>
        /// Register global filters.
        /// </summary>
        /// <param name="filters"></param>
        protected void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            if (filters == null) { throw new ArgumentNullException("filters"); }

            filters.Add(new HandleErrorAttribute());
        }

        /// <summary>
        /// Register routes.
        /// </summary>
        /// <param name="routes"></param>
        protected void RegisterRoutes(RouteCollection routes)
        {
            if (routes == null) { throw new ArgumentNullException("routs"); }

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
        }
        #endregion

        #region RegisterExtension...
        /// <summary>
        /// Responsible for registering loosely-coupled classes.
        /// </summary>
        public static class RegisterExtension
        {
            #region Variables...
            public static IUnityContainer Container { get; internal set; }
            #endregion

            #region Methods...
            /// <summary>
            /// Registers classes.
            /// </summary>
            public static void Register()
            {
                var container = new UnityContainer();
                var section = ConfigurationManager.GetSection("unity") as UnityConfigurationSection;

                if (section != null && section.Containers.Count > 0)
                {
                    section.Configure(container);
                }

                Container = container;

                Container.RegisterType<IProfileRepository, ProfileRepository>();
                Container.RegisterType<IRaceRepository, RaceRepository>();
                Container.RegisterType<IRacePerHorseRepository, RacePerHorseRepository>();
            }
            #endregion
        }
        #endregion  
    }
}
