﻿using AmazingRace.Race.Model.Parameters;
using AmazingRace.Race.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Services.Host.IIS.Controllers
{
    /// <summary>
    /// The controller for the race service.
    /// </summary>
    [RoutePrefix("Race-service")]
    public class RaceController : BaseController
    {
        #region Variables...
        private RaceService _RaceService = GetResolvedType<RaceService>();
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the race controller.
        /// </summary>
        public RaceController()
        {

        }

        [Route("GetRace")]
        [HttpPost]
        public async Task<ActionResult> GetRace(LoadRaceParameter parameter)
        {
            if (!ModelState.IsValid) { throw new ArgumentNullException("parameter"); }

            var rc = await this._RaceService.GetRace(parameter);
            return Json(rc, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}