﻿using AmazingRace.Race.Model.Parameters;
using AmazingRace.Race.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Services.Host.IIS.Controllers
{
    /// <summary>
    /// Controller for race per horse service.
    /// </summary>
    [RoutePrefix("RacePerHorse-service")]
    public class RacePerHorseController : BaseController
    {
        #region Variables...
        private RacePerHorseService _RacePerHorseService = GetResolvedType<RacePerHorseService>();
        #endregion

        #region Methods...
        public RacePerHorseController()
        {

        }

        [Route("GetRacePerHorse")]
        [HttpPost]
        public async Task<ActionResult> GetRacePerHorse(LoadRacePerHorseParameter parameter)
        {
            if (!ModelState.IsValid) { throw new ArgumentNullException("parameter"); }

            var rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            return Json(rc, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}