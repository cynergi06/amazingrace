﻿using AmazingRace.Profile.Model.Parameters;
using AmazingRace.Profile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Services.Host.IIS.Controllers
{
    [RoutePrefix("Profile-service")]
    public class ProfileController : BaseController
    {
        #region Variables...
        private ProfileService _ProfileService = GetResolvedType<ProfileService>();
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the controller.
        /// </summary>
        public ProfileController()
        {
        }

        /// <summary>
        /// Gets the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [Route("GetProfile")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> GetProfile(LoadProfileParameter parameter)
        {
            if (!ModelState.IsValid) { throw new ArgumentNullException("parameter"); }

            var rc = await this._ProfileService.GetProfile(parameter);
            return Json(rc, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Creates the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [Route("CreateProfile")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> CreateProfile(CreateProfileParameter parameter)
        {
            if (!ModelState.IsValid) { throw new ArgumentNullException("parameter"); }

            var rc = await this._ProfileService.CreateProfile(parameter);
            return Json(rc, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            this._ProfileService = null;

            base.Dispose(disposing);
        }
        #endregion
    }
}