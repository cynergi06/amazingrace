﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static Services.Host.IIS.WebApiApplication;
using Unity;

namespace Services.Host.IIS.Controllers
{
    [Authorize]
    [OutputCache(NoStore = true, Duration = 0)]
    public class BaseController : Controller
    {
        protected static T GetResolvedType<T>()
        {
            return RegisterExtension.Container.Resolve<T>();
        }
    }
}