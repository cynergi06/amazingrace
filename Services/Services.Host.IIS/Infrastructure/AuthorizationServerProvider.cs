﻿using AmazingRace.Profile.DataContext;
using AmazingRace.Profile.Model;
using AmazingRace.Profile.Model.Parameters;
using AmazingRace.Profile.Service;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Services.Host.IIS.Infrastructure
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// Called to validate that the origin of the request is a registered "client_id",
        /// and that the correct credentials for that client are present on the request.
        /// If the web application accepts Basic authentication credentials, context.TryGetBasicCredentials(out
        /// clientId, out clientSecret) may be called to acquire those values if present
        /// in the request header. If the web application accepts "client_id" and "client_secret"
        /// as form encoded POST parameters, context.TryGetFormCredentials(out clientId,
        /// out clientSecret) may be called to acquire those values if present in the
        /// request body.  If context.Validated is not called the request will not proceed
        /// further.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        /// <summary>
        /// Called when a request to the Token endpoint arrives with a "grant_type" of
        /// "password". This occurs when the user has provided name and password credentials
        /// directly into the client application's user interface, and the client application
        /// is using those to acquire an "access_token" and optional "refresh_token".
        /// If the web application supports the resource owner credentials grant type
        /// it must validate the context.Username and context.Password as appropriate.
        /// To issue an access token the context.Validated must be called with a new
        /// ticket containing the claims about the resource owner which should be associated
        /// with the access token. The application should take appropriate measures to
        /// ensure that the endpoint isn’t abused by malicious callers.  The default
        /// behavior is to reject this grant type.  See also http://tools.ietf.org/html/rfc6749#section-4.3.2
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            LoadProfileParameter profileParameter = new LoadProfileParameter()
            {
                  EnterpriseId = context.UserName
                , EnterpriseToken = context.Password
            };
            
            Profile profile = null;

            try
            {
                ProfileService profileService = new ProfileService(new ProfileRepository());
                profile = await profileService.GetProfile(profileParameter);


                if (profile != null)
                {
                    identity.AddClaim(new Claim("FirstName", profile.FirstName));
                    identity.AddClaim(new Claim("MiddleName", profile.MiddleName));
                    identity.AddClaim(new Claim("LastName", profile.LastName));
                    identity.AddClaim(new Claim("EnterpriseId", profile.EnterpriseId));
                    identity.AddClaim(new Claim("IsActive", profile.IsActive));
                    identity.AddClaim(new Claim("CreateDate", profile.CreateDate.ToShortDateString()));
                    identity.AddClaim(new Claim("UpdateBy", profile.UpdateBy));
                    identity.AddClaim(new Claim("RoleKey", profile.RoleKey.ToString()));
                    identity.AddClaim(new Claim("RoleName", profile.RoleName));
                    identity.AddClaim(new Claim("RoleDescription", profile.RoleDescription));

                    context.Validated(identity);
                }
            }
            catch (Exception ex)
            {
                context.Rejected();
            }
        }

        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
        }
    }
}