﻿using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Services.Host.IIS.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(Services.Host.IIS.OwinStartup))]
namespace Services.Host.IIS
{
    public class OwinStartup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            //enable cors origin requests
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                  AllowInsecureHttp = true
                , TokenEndpointPath = new PathString("/Authorization-service/GetToken")
                , AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(60)
                , Provider = new AuthorizationServerProvider()
                , RefreshTokenProvider = new AuthenticationTokenProvider()
            };
            app.UseOAuthAuthorizationServer(options);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }
    }
}