﻿using AmazingRace.Profile.DataContext;
using AmazingRace.Profile.Model.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Service.Test
{
    [TestClass]
    public class ProfileServiceTest
    {
        private ProfileService _ProfileService = new ProfileService(new ProfileRepository());

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task ProfileService_GetProfile_WithNullParameter()
        {
            await this._ProfileService.GetProfile(null);
        }

        [TestMethod]
        public async Task ProfileService_GetProfile_InvalidCredentials()
        {
            LoadProfileParameter parameter = new LoadProfileParameter()
            {
                  EnterpriseId = "charles.evan.f.y.yu@accenture.com"
                , EnterpriseToken = "12345678"
            };

            var invalidProfile = await this._ProfileService.GetProfile(parameter);
            Assert.IsNull(invalidProfile, "Profile is valid. Please update parameters.");
        }

        [TestMethod]
        public async Task ProfileService_GetProfile_ValidCredentials()
        {
            LoadProfileParameter parameter = new LoadProfileParameter()
            {
                  EnterpriseId = "charlesyu@yahoo.com"
                , EnterpriseToken = "12345678"

            };

            var validProfile = await this._ProfileService.GetProfile(parameter);
            Assert.IsNotNull(validProfile, "Profile is invalid. Please update parameters.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task ProfileService_CreateProfile_WithNullCreateProfileParameter()
        {
            CreateProfileParameter parameter = null;
            var createProfile = await this._ProfileService.CreateProfile(parameter);
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public async Task ProfileService_CreateProfile_WithNoEnterpriseId()
        {
            CreateProfileParameter parameter = new CreateProfileParameter()
            {
                  EnterpriseId = ""
                , EnterpriseToken = "12345678"
                , FirstName = "Unit Test First Name"
                , MiddleName = "Unit Test Middle Name"
                , LastName = "Unit Test Last Name"
                , RoleKey = 0
                , UpdateBy = "unittest@yahoo.com"
            };

            var id = await this._ProfileService.CreateProfile(parameter);
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public async Task ProfileService_CreateProfile_WithNoEnterpriseToken()
        {
            CreateProfileParameter parameter = new CreateProfileParameter()
            {
                  EnterpriseId = "unittest1firstname@yahoo.com"
                , EnterpriseToken = ""
                , FirstName = "Unit Test First Name"
                , MiddleName = "Unit Test Middle Name"
                , LastName = "Unit Test Last Name"
                , RoleKey = 0
                , UpdateBy = "unittest@yahoo.com"
            };

            var id = await this._ProfileService.CreateProfile(parameter);
        }

        [TestMethod]
        [ExpectedException(typeof(SqlException))]
        public async Task ProfileService_CreateProfile_WithInvalidRoleKey()
        {
            CreateProfileParameter parameter = new CreateProfileParameter()
            {
                  EnterpriseId = "unittest1firstname@yahoo.com"
                , EnterpriseToken = "12345678"
                , FirstName = "Unit Test First Name"
                , MiddleName = "Unit Test Middle Name"
                , LastName = "Unit Test Last Name"
                , RoleKey = 100
                , UpdateBy = "unittest@yahoo.com"
            };

            var id = await this._ProfileService.CreateProfile(parameter);
        }

        [TestMethod]
        public async Task ProfileService_CreateProfile_WithValidParameters()
        {
            CreateProfileParameter parameter = new CreateProfileParameter()
            {
                  EnterpriseId = "unittest" + Guid.NewGuid().ToString() + "@yahoo.com"
                , EnterpriseToken = "12345678"
                , FirstName = "Unit Test First Name"
                , MiddleName = "Unit Test Middle Name"
                , LastName = "Unit Test Last Name"
                , RoleKey = 1
                , UpdateBy = "unittest@yahoo.com"
            };

            var id = await this._ProfileService.CreateProfile(parameter);
            Assert.IsTrue(id > 0, "Failed to create profile.");
        }
    }
}
