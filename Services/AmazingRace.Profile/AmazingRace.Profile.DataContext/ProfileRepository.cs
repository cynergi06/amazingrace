﻿using AmazingRace.Profile.Model;
using AmazingRace.Profile.Model.Entities;
using AmazingRace.Profile.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.DataContext
{
    /// <summary>
    /// The data context for profile.
    /// </summary>
    public class ProfileRepository : DbContext, IProfileRepository
    {
        #region Variables...
        const string CONNECTION_STRING = "AmazingRace";
        private int _DatabaseTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["Database-Timeout"]);
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the profile repository.
        /// </summary>
        static ProfileRepository()
        {
            Database.SetInitializer<ProfileRepository>(null);
        }

        /// <summary>
        /// Initializes the profile repository.
        /// </summary>
        public ProfileRepository() : base(CONNECTION_STRING)
        {
            Database.CommandTimeout = this._DatabaseTimeout;
        }
        #endregion

        #region IProfileRepository...
        /// <summary>
        /// Get the data of the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<Model.Profile> GetProfile(LoadProfileParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            using (var db = new ProfileRepository())
            {
                var rc = await db.Database.SqlQuery<AmazingRace.Profile.Model.Profile>(
                      "EXECUTE dbo.LoadProfile_SP"
                    + "   @EnterpriseId = @EnterpriseId"
                    + " , @EnterpriseToken = @EnterpriseToken"

                    , new SqlParameter("@EnterpriseId", parameter.EnterpriseId)
                    , new SqlParameter("@EnterpriseToken", parameter.EnterpriseToken)
                ).FirstOrDefaultAsync();

                return rc;
            }
        }

        /// <summary>
        /// Creates the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<long> CreateProfile(CreateProfileParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            using (var db = new ProfileRepository())
            {
                var rc = await db.Database.SqlQuery<long>(
                    "EXECUTE dbo.CreateProfile_SP"
                    + "   @FirstName = @FirstName"
                    + " , @MiddleName = @MiddleName"
                    + " , @LastName = @LastName"
                    + " , @EnterpriseId = @EnterpriseId"
                    + " , @EnterpriseToken = @EnterpriseToken"
                    + " , @UpdateBy = @UpdateBy"
                    + " , @RoleKey = @RoleKey"

                    , new SqlParameter("@FirstName", parameter.FirstName)
                    , new SqlParameter("@MiddleName", parameter.MiddleName)
                    , new SqlParameter("@LastName", parameter.LastName)
                    , new SqlParameter("@EnterpriseId", parameter.EnterpriseId)
                    , new SqlParameter("@EnterpriseToken", parameter.EnterpriseToken)
                    , new SqlParameter("@UpdateBy", parameter.UpdateBy)
                    , new SqlParameter("@RoleKey", parameter.RoleKey)
                ).FirstOrDefaultAsync();

                return rc;
            }
        }
        #endregion
    }
}
