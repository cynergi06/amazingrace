﻿using AmazingRace.Profile.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Model.Entities
{
    /// <summary>
    /// Contains methods for profiles.
    /// </summary>
    public interface IProfileRepository
    {
        /// <summary>
        /// Gets the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task<Profile> GetProfile(LoadProfileParameter parameter);

        /// <summary>
        /// Creates the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task<long> CreateProfile(CreateProfileParameter parameter);
    }
}
