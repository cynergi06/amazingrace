﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Model.Parameters
{
    /// <summary>
    /// Parameter to be used when creating profile.
    /// </summary>
    public class CreateProfileParameter
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the enterprise id.
        /// </summary>
        public string EnterpriseId { get; set; }

        /// <summary>
        /// Gets or sets the enterprise token.
        /// </summary>
        public string EnterpriseToken { get; set; }

        /// <summary>
        /// Gets or sets the update by
        /// </summary>
        public string UpdateBy { get; set; }

        /// <summary>
        /// Gets or sets the role key.
        /// </summary>
        public int RoleKey { get; set; }
    }
}
