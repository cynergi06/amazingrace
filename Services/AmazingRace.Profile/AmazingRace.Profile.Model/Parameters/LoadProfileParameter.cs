﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Model.Parameters
{
    /// <summary>
    /// Gets or sets the profile parameter.
    /// </summary>
    public class LoadProfileParameter
    {
        /// <summary>
        /// Gets or sets the enterprise id.
        /// </summary>
        public string EnterpriseId { get; set; }

        /// <summary>
        /// Gets or sets the enterprise token.
        /// </summary>
        public string EnterpriseToken { get; set; }
    }
}
