﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Model
{
    /// <summary>
    /// Gets or sets the profile of the application.
    /// </summary>
    public class Profile
    {
        /// <summary>
        /// Gets or sets the profile key.
        /// </summary>
        public long ProfileKey { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name.
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the enterprise id.
        /// </summary>
        public string EnterpriseId { get; set; }

        /// <summary>
        /// Gets or sets the status of the profile.
        /// </summary>
        public string IsActive { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        public System.DateTime CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the update by.
        /// </summary>
        public string UpdateBy { get; set; }

        /// <summary>
        /// Gets or sets the update date.
        /// </summary>
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// Gets or sets the role key.
        /// </summary>
        public int RoleKey { get; set; }

        /// <summary>
        /// Gets or sets the role name.
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// Gets or sets the role description.
        /// </summary>
        public string RoleDescription { get; set; }
    }
}
