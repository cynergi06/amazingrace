﻿using AmazingRace.Profile.Model.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Profile.Service
{
    /// <summary>
    /// The service for profiles.
    /// </summary>
    public class ProfileService : IDisposable
    {
        #region Variables...
        private IProfileRepository _ProfileRepository = null;
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the profile service.
        /// </summary>
        /// <param name="profileRepository"></param>
        public ProfileService(IProfileRepository profileRepository)
        {
            this._ProfileRepository = profileRepository;
        }

        /// <summary>
        /// Gets the profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<AmazingRace.Profile.Model.Profile> GetProfile(AmazingRace.Profile.Model.Parameters.LoadProfileParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            var rc = await this._ProfileRepository.GetProfile(parameter);
            return rc;
        }

        /// <summary>
        /// Creates profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<long> CreateProfile(AmazingRace.Profile.Model.Parameters.CreateProfileParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            var rc = await this._ProfileRepository.CreateProfile(parameter);
            return rc;
        }
        #endregion

        #region IDisposable...
        /// <summary>
        /// Disposes the object.
        /// </summary>
        public void Dispose()
        {
            this._ProfileRepository = null;
        }
        #endregion  
    }
}
