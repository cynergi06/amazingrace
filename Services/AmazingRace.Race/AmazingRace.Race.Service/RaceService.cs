﻿using AmazingRace.Race.Model.Entities;
using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Service
{
    /// <summary>
    /// The service that contains method regarding race.
    /// </summary>
    public class RaceService : IDisposable
    {
        #region Variables...
        private IRaceRepository _RaceRepository = null;
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the race service.
        /// </summary>
        /// <param name=""></param>
        public RaceService(IRaceRepository raceRepository)
        {
            this._RaceRepository = raceRepository;
        }

        public async Task<List<AmazingRace.Race.Model.Race>> GetRace(LoadRaceParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            var rc = await this._RaceRepository.GetRace(parameter);
            return rc;
        }
        #endregion

        #region IDisposable...
        public void Dispose()
        {
            this._RaceRepository = null;
        }
        #endregion  
    }
}
