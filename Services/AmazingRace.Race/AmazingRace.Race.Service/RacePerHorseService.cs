﻿using AmazingRace.Race.Model.Entities;
using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Service
{
    /// <summary>
    /// The service that contains method regarding race.
    /// </summary>
    public class RacePerHorseService : IDisposable
    {
        #region Variables...
        private IRacePerHorseRepository _RacePerHorseRepository = null;
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the race service.
        /// </summary>
        /// <param name=""></param>
        public RacePerHorseService(IRacePerHorseRepository racePerHorseRepository)
        {
            this._RacePerHorseRepository = racePerHorseRepository;
        }

        public async Task<List<AmazingRace.Race.Model.RacePerHorse>> GetRacePerHorse(LoadRacePerHorseParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            var rc = await this._RacePerHorseRepository.GetRacePerHorse(parameter);
            return rc;
        }
        #endregion

        #region IDisposable...
        public void Dispose()
        {
            this._RacePerHorseRepository = null;
        }
        #endregion  
    }
}
