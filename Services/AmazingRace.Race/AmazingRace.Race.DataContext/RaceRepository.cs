﻿using AmazingRace.Race.Model;
using AmazingRace.Race.Model.Entities;
using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.DataContext
{
    /// <summary>
    /// The database repository of the race.
    /// </summary>
    public class RaceRepository : DbContext, IRaceRepository
    {
        #region Variables...
        const string CONNECTION_STRING = "AmazingRace";
        private int _DatabaseTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["Database-Timeout"]);
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the race repository.
        /// </summary>
        static RaceRepository()
        {
            Database.SetInitializer<RaceRepository>(null);
        }

        /// <summary>
        /// Initializes the race repository.
        /// </summary>
        public RaceRepository() : base(CONNECTION_STRING)
        {
            Database.CommandTimeout = this._DatabaseTimeout;
        }
        #endregion

        #region IRaceRepository...
        /// <summary>
        /// Implement get race method.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public async Task<List<Model.Race>> GetRace(LoadRaceParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            using (var db = new RaceRepository())
            {
                var rc = await db.Database.SqlQuery<Race.Model.Race>(
                      "EXECUTE dbo.LoadRace_SP"
                    + "   @RaceKey = @RaceKey"
                    + " , @RaceName = @RaceName"
                    + " , @RaceStatusKey = @RaceStatusKey"

                    , new SqlParameter("@RaceKey", parameter.RaceKey)
                    , new SqlParameter("@RaceName", parameter.RaceName)
                    , new SqlParameter("@RaceStatusKey", parameter.RaceStatusKey)
                ).ToListAsync();

                return rc;
            }
        }
        #endregion
    }
}
