﻿using AmazingRace.Race.Model;
using AmazingRace.Race.Model.Entities;
using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.DataContext
{
    /// <summary>
    /// The database repository for race per horse.
    /// </summary>
    public class RacePerHorseRepository : DbContext, IRacePerHorseRepository
    {
        #region Variables...
        const string CONNECTION_STRING = "AmazingRace";
        private int _DatabaseTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["Database-Timeout"]);
        #endregion

        #region Methods...
        /// <summary>
        /// Initializes the race per horse repository.
        /// </summary>
        static RacePerHorseRepository()
        {
            Database.SetInitializer<RacePerHorseRepository>(null);
        }

        /// <summary>
        /// Initializes the race per horse repository.
        /// </summary>
        public RacePerHorseRepository() : base(CONNECTION_STRING)
        {
            Database.CommandTimeout = this._DatabaseTimeout;
        }
        #endregion

        #region IRacePerHorseRepository
        public async Task<List<RacePerHorse>> GetRacePerHorse(LoadRacePerHorseParameter parameter)
        {
            if (parameter == null) { throw new ArgumentNullException("parameter"); }

            using (var db = new RacePerHorseRepository())
            {
                var rc = await db.Database.SqlQuery<RacePerHorse>(
                      "EXECUTE dbo.LoadRacePerHorse_SP"
                    + "   @RaceKey = @RaceKey"
                    + " , @HorseKey = @HorseKey"

                    , new SqlParameter("@RaceKey", parameter.RaceKey)
                    , new SqlParameter("@HorseKey", parameter.HorseKey)
                ).ToListAsync();

                return rc;
            }
        }
        #endregion
    }
}
