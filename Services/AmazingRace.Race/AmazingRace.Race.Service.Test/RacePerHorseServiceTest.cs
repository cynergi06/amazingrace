﻿using AmazingRace.Race.DataContext;
using AmazingRace.Race.Model.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Service.Test
{
    [TestClass]
    public class RacePerHorseServiceTest
    {
        private RacePerHorseService _RacePerHorseService = new RacePerHorseService(new RacePerHorseRepository());

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task RacePerHorseService_GetRacePerHorsePerHorse_WithNullParameter()
        {
            await this._RacePerHorseService.GetRacePerHorse(null);
        }

        [TestMethod]
        public async Task RacePerHorseService_GetRacePerHorse_FilterRaceKeyNoResult()
        {
            LoadRacePerHorseParameter parameter = new LoadRacePerHorseParameter()
            {
                  RaceKey = -1
                , HorseKey = 0
            };

            List<Race.Model.RacePerHorse> rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            Assert.IsTrue(rc.Count == 0, "Race key exist.");
        }

        [TestMethod]
        public async Task RacePerHorseService_GetRacePerHorse_FilterHorseKeyNoResult()
        {
            LoadRacePerHorseParameter parameter = new LoadRacePerHorseParameter()
            {
                  RaceKey = 0
                , HorseKey = -1
            };

            List<Race.Model.RacePerHorse> rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            Assert.IsTrue(rc.Count == 0, "Horse key exist.");
        }

        [TestMethod]
        public async Task RacePerHorseService_GetRacePerHorse_GetSpecificRaceKey()
        {
            LoadRacePerHorseParameter parameter = new LoadRacePerHorseParameter()
            {
                  RaceKey = 2
                , HorseKey = 0
            };

            List<Race.Model.RacePerHorse> rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            Assert.IsTrue(rc.Count > 0, "Race key does not exist.");
        }

        [TestMethod]
        public async Task RacePerHorseService_GetRacePerHorse_GetSpecificHorseKey()
        {
            LoadRacePerHorseParameter parameter = new LoadRacePerHorseParameter()
            {
                  RaceKey = 0
                , HorseKey = 4
            };

            List<Race.Model.RacePerHorse> rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            Assert.IsTrue(rc.Count > 0, "Horse key does not exist.");
        }

        [TestMethod]
        public async Task RacePerHorseService_GetRacePerHorse_GetAllResult()
        {
            LoadRacePerHorseParameter parameter = new LoadRacePerHorseParameter()
            {
                  RaceKey = 0
                , HorseKey = 0
            };

            List<Race.Model.RacePerHorse> rc = await this._RacePerHorseService.GetRacePerHorse(parameter);
            Assert.IsTrue(rc.Count > 0, "No data available");
        }
    }
}
