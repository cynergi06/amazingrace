﻿using AmazingRace.Race.DataContext;
using AmazingRace.Race.Model.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Service.Test
{
    [TestClass]
    public class RaceServiceTest
    {
        private RaceService _RaceService = new RaceService(new RaceRepository());

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task RaceService_GetRace_WithNullParameter()
        {    
            await _RaceService.GetRace(null);
        }

        [TestMethod]
        public async Task RaceService_GetRace_FilterRaceKeyNoResult()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = -1
                , RaceName = ""
                , RaceStatusKey = 0
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count == 0, "Race key exist.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_FilterRaceNameNoResult()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 0
                , RaceName = "Invalid Race Name"
                , RaceStatusKey = 0
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count == 0, "Race name exist.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_FilterRaceStatusKeyNoResult()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 0
                , RaceName = ""
                , RaceStatusKey = 100
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count == 0, "Race status key exist.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_GetSpecificRaceKey()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 2
                , RaceName = ""
                , RaceStatusKey = 0
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count == 1, "Race key does not exist or result is more than 1.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_GetSpecificRaceName()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 0
                , RaceName = "Race 1 - Pending"
                , RaceStatusKey = 0
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count == 1, "Race name does not exit or result is more than 1.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_GetSpecificRaceStatusKey()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 0
                , RaceName = ""
                , RaceStatusKey = 2
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count > 0, "Race status key does not exist.");
        }

        [TestMethod]
        public async Task RaceService_GetRace_GetAllResult()
        {
            LoadRaceParameter parameter = new LoadRaceParameter()
            {
                  RaceKey = 0
                , RaceName = ""
                , RaceStatusKey = 0
            };

            List<Race.Model.Race> rc = await this._RaceService.GetRace(parameter);
            Assert.IsTrue(rc.Count > 0, "No data available");
        }
    }
}
