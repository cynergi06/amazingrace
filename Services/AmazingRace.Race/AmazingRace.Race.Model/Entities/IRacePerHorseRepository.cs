﻿using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model.Entities
{
    /// <summary>
    /// Contains methods related to race per horse.
    /// </summary>
    public interface IRacePerHorseRepository
    {
        Task<List<RacePerHorse>> GetRacePerHorse(LoadRacePerHorseParameter parameter);
    }
}
