﻿using AmazingRace.Race.Model.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model.Entities
{
    /// <summary>
    /// Contains the methods for the race.
    /// </summary>
    public interface IRaceRepository
    {
        /// <summary>
        /// Gets the list of races of the application.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        Task<List<Race>> GetRace(LoadRaceParameter parameter);
    }
}
