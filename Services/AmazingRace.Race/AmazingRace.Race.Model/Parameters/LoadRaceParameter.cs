﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model.Parameters
{
    /// <summary>
    /// The parameter to get the list of races.
    /// </summary>
    public class LoadRaceParameter
    {
        /// <summary>
        /// Gets or sets the race key.
        /// </summary>
        public long RaceKey { get; set; }

        /// <summary>
        /// Gets or sets the race name.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string RaceName { get; set; }

        /// <summary>
        /// Gets or sets the race status key.
        /// </summary>
        public long RaceStatusKey { get; set; }
    }
}
