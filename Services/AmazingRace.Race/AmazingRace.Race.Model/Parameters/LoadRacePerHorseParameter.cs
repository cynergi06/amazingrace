﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model.Parameters
{
    /// <summary>
    /// Parameter to load the data race per horse.
    /// </summary>
    public class LoadRacePerHorseParameter
    {
        /// <summary>
        /// Gets or sets the race key.
        /// </summary>
        public long RaceKey { get; set; }

        /// <summary>
        /// Gets or sets the horse key.
        /// </summary>
        public long HorseKey { get; set; }
    }
}
