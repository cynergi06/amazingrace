﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model
{
    /// <summary>
    /// Model for race per horse.
    /// </summary>
    public class RacePerHorse
    {
        /// <summary>
        /// Gets or sets the race key.
        /// </summary>
        public long RaceKey{ get; set; }

        /// <summary>
        /// Gets or sets the race name.
        /// </summary>
        public string RaceName{ get; set; }

        /// <summary>
        /// Gets or sets the race date.
        /// </summary>
        public DateTime RaceDate{ get; set; }

        /// <summary>
        /// Gets or sets the race status key.
        /// </summary>
        public long RaceStatusKey{ get; set; }

        /// <summary>
        /// Gets or sets the race status.
        /// </summary>
        public string RaceStatus{ get; set; }

        /// <summary>
        /// Gets or sets the horse key.
        /// </summary>
        public long HorseKey{ get; set; }

        /// <summary>
        /// Gets or sets the horse name.
        /// </summary>
        public string HorseName{ get; set; }

        /// <summary>
        /// Gets or sets the odds.
        /// </summary>
        public decimal Odds{ get; set; }

        /// <summary>
        /// Gets or sets the total amount of the horse.
        /// </summary>
        public decimal? TotalAmount{ get; set; }

        /// <summary>
        /// Gets or sets the total bets of the horse.
        /// </summary>
        public int? TotalBets { get; set; }
    }
}
