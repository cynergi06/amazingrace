﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazingRace.Race.Model
{
    /// <summary>
    /// Model of the race.
    /// </summary>
    public class Race
    {
        /// <summary>
        /// Gets or sets the race key.
        /// </summary>
        public long RaceKey { get; set; }

        /// <summary>
        /// Gets or sets the race name.
        /// </summary>
        public string RaceName { get; set; }

        /// <summary>
        /// Gets or sets the race date.
        /// </summary>
        public System.DateTime RaceDate { get; set; }

        /// <summary>
        /// Gets or sets the race status key.
        /// </summary>
        public long RaceStatusKey { get; set; }

        /// <summary>
        /// Gets or sets the race status.
        /// </summary>
        public string RaceStatus { get; set; }

        /// <summary>
        /// Gets or sets the total amount of bets.
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Gets or sets the status of the race.
        /// </summary>
        public string IsActive { get; set; }

        /// <summary>
        /// Gets or sets the create date.
        /// </summary>
        public System.DateTime CreateDate { get; set; }

        /// <summary>
        /// Gets or sets the update by.
        /// </summary>
        public string UpdateBy { get; set; }

        /// <summary>
        /// Gets or sets the update date.
        /// </summary>
        public DateTime? UpdateDate { get; set; }
    }
}
