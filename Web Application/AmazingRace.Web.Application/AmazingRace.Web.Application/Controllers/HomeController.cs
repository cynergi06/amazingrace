﻿using AmazingRace.Web.Application.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AmazingRace.Web.Application.Controllers
{
    public class HomeController : Controller
    {
        #region Variables...
        private string Service_Base_Url = ConfigurationManager.AppSettings["Service-Base-Url"];
        #endregion

        #region Methods...
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Log-in profile.
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        [Route("LogIn")]
        [HttpPost]
        public async Task<ActionResult> LogIn(LogInParameter parameter)
        {
            if (!ModelState.IsValid) { throw new ArgumentNullException("parameter"); }

            Session["Profile"] = null;
            Models.Profile profile = null;

            using (HttpClient profileClient = new HttpClient())
            {
                profileClient.BaseAddress = new Uri(Service_Base_Url);
                profileClient.DefaultRequestHeaders.Clear();

                StringContent content = new StringContent(
                      JsonConvert.SerializeObject(parameter)
                    , Encoding.UTF8
                    , "application/json");

                HttpResponseMessage response = profileClient.PostAsync("Profile-service/GetProfile", content).Result;
                string responseBody = await response.Content.ReadAsStringAsync();
                profile = JsonConvert.DeserializeObject<Profile>(responseBody);

                Session["Profile"] = profile;
                Session["EnterpriseToken"] = parameter.EnterpriseToken;

                if (Session["Profile"] != null)
                {
                    Session["Token"] = this.GetToken(null);
                }
            }

            return Json(profile, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets the current profile in session.
        /// </summary>
        /// <returns></returns>
        [Route("GetProfile")]
        [HttpGet]
        public async Task<ActionResult> GetProfile()
        {
            return Json(Session["Profile"], JsonRequestBehavior.AllowGet);
        }

        [Route("GetToken")]
        [HttpPost]
        public async Task<ActionResult> GetToken(SecurityToken token)
        {
            SecurityToken rc = null;

            if (token == null || token.Expires <= DateTime.UtcNow)
            {
                if (Session != null)
                {
                    if (Session["Profile"] != null && Session["EnterpriseToken"] != null)
                    {
                        Profile profile = (Profile)Session["Profile"];
                        string enterpriseToken = Session["EnterpriseToken"].ToString();

                        using (HttpClient tokenClient = new HttpClient())
                        {
                            tokenClient.BaseAddress = new Uri(Service_Base_Url);
                            tokenClient.DefaultRequestHeaders.Clear();

                            StringContent content = new StringContent(
                                string.Format(
                                    "grant_type={0}&username={1}&password={2}"
                                    , HttpUtility.UrlEncode("password")
                                    , HttpUtility.UrlEncode(profile.EnterpriseId)
                                    , HttpUtility.UrlEncode(enterpriseToken)
                                )
                                , Encoding.UTF8
                                , "application/x-www-form-urlencoded"
                            );

                            HttpResponseMessage response = tokenClient.PostAsync("Authorization-service/GetToken", content).Result;
                            string responseBody = await response.Content.ReadAsStringAsync();
                            rc = JsonConvert.DeserializeObject<SecurityToken>(responseBody);

                            Session["SecurityToken"] = rc;
                        }
                    }
                }
            }

            return Json(rc, JsonRequestBehavior.AllowGet);
        }
    }
    #endregion
}