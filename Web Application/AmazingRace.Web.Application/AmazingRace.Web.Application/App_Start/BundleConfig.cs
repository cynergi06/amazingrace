﻿using System;
using System.Configuration;
using System.Globalization;
using System.Web;
using System.Web.Optimization;

namespace AmazingRace.Web.Application
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleConfig.BundleLibraryFiles();
            BundleConfig.BundleSharedFiles();
            BundleConfig.BundleHomeFiles();
            BundleConfig.BundleDashboardFiles();

            BundleTable.EnableOptimizations = Convert.ToBoolean(ConfigurationManager.AppSettings["Enable-Bundling"]);
        }

        /// <summary>
        /// Bundles library scripts.
        /// </summary>
        public static void BundleLibraryFiles()
        {
            var libraryScriptBundle = new ScriptBundle(string.Format(CultureInfo.InvariantCulture, "~/library/scripts"));
            libraryScriptBundle.Include("~/Scripts/jquery-3.2.1.min.js");
            libraryScriptBundle.Include("~/Scripts/angular.js");
            libraryScriptBundle.Include("~/Scripts/angular-route.min.js");
            libraryScriptBundle.Include("~/Scripts/angular-animate.min.js");
            libraryScriptBundle.Include("~/Scripts/angular-cookies.min.js");
            libraryScriptBundle.Include("~/Scripts/bootstrap.min.js");
            libraryScriptBundle.Include("~/Scripts/moment.min.js");
            libraryScriptBundle.Include("~/Scripts/toastr.min.js");
            libraryScriptBundle.Include("~/Scripts/ag-grid.min.js");

            BundleTable.Bundles.Add(libraryScriptBundle);

            var libraryStyleBundle = new StyleBundle(string.Format(CultureInfo.InvariantCulture, "~/library/styles"));
            libraryStyleBundle.Include("~/Content/site.css");
            libraryStyleBundle.Include("~/Content/bootstrap.min.css");
            libraryStyleBundle.Include("~/Content/font-awesome.min.css");
            libraryStyleBundle.Include("~/Content/toastr.min.css");

            BundleTable.Bundles.Add(libraryStyleBundle);
        }

        public static void BundleSharedFiles()
        {
            var sharedScriptBundle = new ScriptBundle(string.Format(CultureInfo.InvariantCulture, "~/shared/scripts"));
            sharedScriptBundle.Include("~/Areas/Shared/scripts/app.js");
            sharedScriptBundle.Include("~/Areas/Shared/scripts/app.constants.js");
            sharedScriptBundle.Include("~/Areas/Shared/scripts/base.controller.js");
            sharedScriptBundle.Include("~/Areas/Shared/scripts/base.service.js");

            BundleTable.Bundles.Add(sharedScriptBundle);
        }

        public static void BundleHomeFiles()
        {
            var homeScriptBundle = new ScriptBundle(string.Format(CultureInfo.InvariantCulture, "~/home/scripts"));
            homeScriptBundle.Include("~/Areas/Home/scripts/home.controller.js");
            homeScriptBundle.Include("~/Areas/Home/scripts/home.service.js");

            BundleTable.Bundles.Add(homeScriptBundle);

            var homeStyleBundle = new StyleBundle(string.Format(CultureInfo.InvariantCulture, "~/home/styles"));
            homeStyleBundle.Include("~/Areas/Home/styles/home.css");

            BundleTable.Bundles.Add(homeStyleBundle);
        }

        public static void BundleDashboardFiles()
        {
            var dashboardScriptBundle = new ScriptBundle(string.Format(CultureInfo.InvariantCulture, "~/dashboard/scripts"));
            dashboardScriptBundle.Include("~/Areas/Dashboard/scripts/dashboard.controller.js");
            dashboardScriptBundle.Include("~/Areas/Dashboard/scripts/dashboard.service.js");

            BundleTable.Bundles.Add(dashboardScriptBundle);

            var dashboardStyleBundle = new StyleBundle(string.Format(CultureInfo.InvariantCulture, "~/dashboard/styles"));
            dashboardStyleBundle.Include("~/Areas/Dashboard/styles/dashboard.css");

            BundleTable.Bundles.Add(dashboardStyleBundle);
        }
    }
}
