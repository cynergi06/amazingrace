﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazingRace.Web.Application.Models
{
    /// <summary>
    /// Generates the security token to be used for the application
    /// </summary>
    public class SecurityToken
    {
        /// <summary>
        /// Gets or sets access token.
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        /// <summary>
        /// Gets or sets token type.
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        /// <summary>
        /// Gets or sets expires in.
        /// </summary>
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        /// <summary>
        /// Gets or sets refresh token.
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        /// <summary>
        /// Gets or sets the issued date.
        /// </summary>
        [JsonProperty(".issued")]
        public DateTime Issued { get; set; }
        /// <summary>
        /// Gets or sets the expire date.
        /// </summary>
        [JsonProperty(".expires")]
        public DateTime Expires { get; set; }
    }
}