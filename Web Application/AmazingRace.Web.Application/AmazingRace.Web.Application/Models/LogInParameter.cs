﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazingRace.Web.Application.Models
{
    /// <summary>
    /// Parameter when logging in.
    /// </summary>
    public class LogInParameter
    {
        /// <summary>
        /// Gets or sets the enterprise id.
        /// </summary>
        public string EnterpriseId { get; set; }

        /// <summary>
        /// Gets or sets enterprise token.
        /// </summary>
        public string EnterpriseToken { get; set; }
    }
}