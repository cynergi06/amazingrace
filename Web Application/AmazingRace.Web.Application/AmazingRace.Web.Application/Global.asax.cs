﻿using AmazingRace.Web.Application.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AmazingRace.Web.Application
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            Response.AppendHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            Response.AppendHeader("X-Xss-Protection", "1; mode=block");

            if (!Request.RawUrl.Contains(".gif") && !Request.RawUrl.Contains(".png"))
            {
                Response.AppendHeader("X-Content-Type-Options", "nosniff");
            }

            Response.AppendHeader("X-Frame-Options", "SAMEORIGIN");
        }
    }
}
