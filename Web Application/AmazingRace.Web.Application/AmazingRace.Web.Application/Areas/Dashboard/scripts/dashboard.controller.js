﻿app.controller('dashboardController', ['$scope', '$window', 'dashboardService',
    function ($scope, $window, dashboardService) {
        $scope.$parent.pageTitle = 'WHA - Avanade - Dashboard';

        var loadRaceParameter = {
              RaceKey: 0
            , RaceName: ''
            , RaceStatusKey: 0
        };

        var loadRacePerHorseParameter = {
              RaceKey: 0
            , HorseKey: 0
        };

        var raceColumnDefinition = [
              { headerName: 'ID', field: 'RaceKey', width: 50 }
            , { headerName: 'Name', field: 'RaceName', width: 150 }
            , { headerName: 'Date of Race', field: 'RaceDate', width: 200, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Status', field: 'RaceStatus', width: 110 }
            , { headerName: 'Amount', field: 'TotalAmount', width: 100, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Date Created', field: 'CreateDate', width: 200, cellStyle: { 'text-align': 'right' } }
        ];

        var racePerHorseColumnDefinition = [
              { headerName: 'Horse Number', field: 'HorseKey', width: 100 }
            , { headerName: 'Horse Name', field: 'HorseName', width: 180 }
            , { headerName: 'Odds', field: 'Odds', width: 50, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Total Amount', field: 'TotalAmount', width: 100, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Total Bets', field: 'TotalBets', width: 75, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Race Id', field: 'RaceKey', width: 50, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Name', field: 'RaceName', width: 150 }
            , { headerName: 'Date/Time of Race', field: 'RaceDate', width: 200, cellStyle: { 'text-align': 'right' } }
            , { headerName: 'Status', field: 'RaceStatus', width: 110 }
        ];

        $scope.raceGridOptions = {
              columnDefs: raceColumnDefinition
            , rowData: null
            , rowSelection: 'single'
            , rowDeselection: true
            , rowHeight: 40
            , virtualPaging: true
            , enableSorting: false
            , suppressMovableColumns: true
            , overlayLoadingTemplate: '<span class="ag-overlay-loading-center IA-loader"><span>Please wait while rows are loading..</span></span>'
        };

        $scope.racePerHorseGridOptions = {
            columnDefs: racePerHorseColumnDefinition
            , rowData: null
            , rowSelection: 'single'
            , rowDeselection: true
            , rowHeight: 40
            , virtualPaging: true
            , enableSorting: false
            , suppressMovableColumns: true
            , overlayLoadingTemplate: '<span class="ag-overlay-loading-center IA-loader"><span>Please wait while rows are loading..</span></span>'
        };

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $scope.getRace = function () {
            dashboardService.getRace(loadRaceParameter).then(
                function (success) {
                    var obj = $.map(success.data, function (item, i) {
                        item.RaceDate = moment(item.RaceDate).format('LLL');
                        item.CreateDate = moment(item.CreateDate).format('LLL');
                    });

                    $scope.raceGridOptions.api.setRowData(success.data);
                }
                , function (error) {
                    toastr.error(error.message);
                }
            )
        };

        $scope.getRacePerHorse = function () {
            dashboardService.getRacePerHorse(loadRacePerHorseParameter).then(
                function (success) {
                    var obj = $.map(success.data, function (item, i) {
                        item.RaceDate = moment(item.RaceDate).format('LLL');
                    });

                    $scope.racePerHorseGridOptions.api.setRowData(success.data);
                }
                , function (error) {
                    toastr.error(error.message);
                }
            )
        };

        var initializeComponent = function () {
            $scope.getRace();
            $scope.getRacePerHorse();
        };

        initializeComponent();
    }
])