﻿app.factory('dashboardService', ['$q', '$http', '$window', 'app.constants',
    function ($q, $http, $window, constants) {
        return {
            getRace: function (parameter) {
                var baseUrl = constants.dashboard.getRace;
                var defer = $q.defer();

                $http.post(baseUrl, parameter, { cache: false }).then(
                    function (success) {
                        var obj = { data: success.data };
                        defer.resolve(obj);
                    }
                    , function (error) {
                        var obj = { data: error.data, message: 'Failed to load race.' };
                        defer.reject(obj);
                    }
                );

                return defer.promise;
            }
            , getRacePerHorse: function (parameter) {
                var baseUrl = constants.dashboard.getRacePerHorse;
                var defer = $q.defer();

                $http.post(baseUrl, parameter, { cache: false }).then(
                    function (success) {
                        var obj = { data: success.data };
                        defer.resolve(obj);
                    }
                    , function (error) {
                        var obj = { data: error.data, message: 'Failed to load race per horse.' };
                        defer.reject(obj);
                    }
                );

                return defer.promise;
            }
        }
    }
])