﻿app.controller('homeController', ['$scope', '$window', 'homeService',
    function ($scope, $window, homeService) {
        $scope.$parent.pageTitle = 'WHA - Amazing Race - Log In';

        $scope.profile = {
              FirstName: ''
            , MiddleName: ''
            , LastName: ''
            , EnterpriseId: ''
            , EnterpriseToken: ''
            , UpdateBy: ''
            , RoleKey: 1
        };

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $scope.createProfile = function () {
            if (
                ($scope.profile.FirstName == undefined || $scope.profile.FirstName == '')
                | ($scope.profile.MiddleName == undefined || $scope.profile.MiddleName == '')
                | ($scope.profile.LastName == undefined || $scope.profile.LastName == '')
                | ($scope.profile.EnterpriseId == undefined || $scope.profile.EnterpriseId == '')
                | ($scope.profile.EnterpriseToken == undefined || $scope.profile.EnterpriseToken == '')
                | ($scope.profile.RoleKey == undefined)) {
                //do nothing since this already have validation in html.
            }
            else {
                $scope.profile.UpdateBy = $scope.profile.EnterpriseId;

                homeService.createProfile($scope.profile).then(
                    function (success) {
                        toastr.success('Successfully created profile. You may now use the account to log-in.');

                        $scope.profile.FirstName = '';
                        $scope.profile.MiddleName = '';
                        $scope.profile.LastName = '';
                        $scope.profile.EnterpriseId = '';
                        $scope.profile.EnterpriseToken = '';
                        $scope.profile.UpdateBy = '';
                        $scope.profile.RoleKey = 1;

                    }
                    , function (error) {
                        toastr.error(error.message);
                    }
                )
            }
        };
    }
]);