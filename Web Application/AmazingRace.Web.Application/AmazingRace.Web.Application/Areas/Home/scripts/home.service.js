﻿app.factory('homeService', ['$q', '$http', '$window', 'app.constants',
    function ($q, $http, $window, constants) {
        return {
            createProfile: function (parameter) {
                var baseUrl = constants.home.createProfile;
                var defer = $q.defer();

                $http.post(baseUrl, parameter, { cache: false }).then(
                    function (success) {
                        defer.resolve(success.data);
                    }
                    , function (error) {
                        var obj = { data: error.data, message: 'Failed to create profile.' };
                        defer.reject(obj);
                    }
                );

                return defer.promise;
            }
        }
    }
]);