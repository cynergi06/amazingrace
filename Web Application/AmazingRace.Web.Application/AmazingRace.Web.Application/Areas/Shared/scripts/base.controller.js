﻿app.controller('baseController', ['$scope', '$window', 'baseService', 
    function ($scope, $window, baseService) {
        $scope.pageTitle = 'WHA - Amazing Race';
        $scope.profile = null;

        $scope.profileParameter = {
              EnterpriseId: ''
            , EnterpriseToken: ''
        };

        $scope.isLoggingIn = false;

        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $scope.login = function () {
            if (
                ($scope.profileParameter.EnterpriseId == undefined || $scope.profileParameter.EnterpriseId == '')
                | ($scope.profileParameter.EnterpriseToken == undefined || $scope.profileParameter.EnterpriseToken == '')) {
                //do nothing since validation is available in html.
            }
            else {
                $scope.isLoggingIn = true;

                baseService.login($scope.profileParameter).then(
                    function (success) {
                        $scope.isLoggingIn = false;
                        $window.location.href = "/dashboard";
                    }
                    , function (error) {
                        $scope.isLoggingIn = false;
                        toastr.error(error.message);
                    }
                );
            }
        };

        var getProfile = function () {
            baseService.getProfile().then(
                function (success) {
                    var obj = success.data;

                    if (obj == undefined || obj == null || obj == "") {
                        if ($window.location.href != BASE_URL + "/") {
                            $window.location.href = BASE_URL;
                        };
                    }
                    else {
                        $scope.profile = obj;
                    };
                }
                , function (error) {
                    toastr.error(error.message);
                }
            );
        };

        $scope.isActiveMenu = function (menu) {
            if ($window.location.href == BASE_URL + "/" + menu) {
                return true;
            }
        };

        $scope.menuItemClicked = function (menu) {
            $window.location.href = BASE_URL + "/" + menu;
        };

        var initializeComponent = function () {
            getProfile();
        };

        initializeComponent();
    }
]);