﻿'use strict'
app.constant('app.constants', {
    base: {
          login: BASE_URL + '/Home/LogIn'
        , getProfile: BASE_URL + '/Home/GetProfile'
    }
    , home: {
        createProfile: SERVICE_BASE_URL + '/Profile-service/CreateProfile'
    }
    , dashboard: {
          getRace: SERVICE_BASE_URL + '/Race-service/GetRace'
        , getRacePerHorse: SERVICE_BASE_URL + '/RacePerHorse-service/GetRacePerHorse'
    }
})