﻿app.factory('baseService', ['$q', '$http', '$window', 'app.constants',
    function ($q, $http, $window, constants) {
        return {
            login: function (profileParameter) {
                var baseUrl = constants.base.login;
                var defer = $q.defer();

                $http.post(baseUrl, profileParameter, { cache: false }).then(
                    function (success) {
                        var obj = { data: success.data };
                        defer.resolve(obj);
                    }
                    , function (error) {
                        var obj = { data: error.data, message: 'Invalid username and/or password.' }
                        defer.reject(obj);
                    }
                );

                return defer.promise;
            }
            , getProfile: function () {
                var baseUrl = constants.base.getProfile;
                var defer = $q.defer();

                $http.get(baseUrl, { cache: false }).then(
                    function (success) {
                        var obj = { data: success.data };
                        defer.resolve(obj);
                    }
                    , function (error) {
                        var obj = { data: error.data, message: 'Failed to get profile.' }
                        defer.reject(obj);
                    }
                );

                return defer.promise;
            }
        }
    }
])