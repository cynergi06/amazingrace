﻿agGrid.initialiseAgGridWithAngular1(angular);
var app = angular.module('amazingRaceApp', ['ngRoute', 'agGrid']);

app.factory('httpInterceptorVersion', function () {
    return {
        request: function (config) {
            /// <summary>
            /// This function will manage the request adding cache false for administrators.
            /// </summary>
            /// <param name="config">Contains interception data for the request.</param>
            /// <returns type="obj">Configuration object.</returns>
            if ((config.method === "GET") && (config.url.match(/\.\html?$/))) {
                if (config.url.indexOf("uib/template/") !== 0) {    //bypass templates of angularUI bootstrap library
                    config.url += '?v=' + CURRENT_VERSION;
                }
            }
            return config;
        }
    };
});

app.factory('tokenService', ['$q', '$injector', 
    function ($q, $injector) {
        return {
            getToken: function () {
                var baseUrl = BASE_URL + '/Home/GetToken';
                var defer = $q.defer();

                $injector.get('$http').post(baseUrl, { cache: false }).then(
                    function (success) {
                        defer.resolve(success.data);
                    }
                );

                return defer.promise;
            }
        }
    }
]);

app.factory('httpInterceptorToken', ['$q', '$injector', 'tokenService',
    function ($q, $injector, tokenService) {
        return {
            request: function (config) {
                if (config.url.indexOf('-service') > -1) {
                    config.headers['X-Requested-With'] = "XMLHttpRequest";
                    config.headers['x-acc-clientid'] = "amazingraceservices";

                    if (typeof jweToken != 'undefined') {
                        config.headers.Authorization = "Bearer " + jweToken.AccessToken;
                    };

                    return config;
                };

                return config;
            }
            , responseError: function (response) {
                if (response.status == 401) {
                    var deferred = $q.defer();
                    var $http = $injector.get('$http');

                    tokenService.getToken().then(
                        function (token) {
                            jweToken = token;
                            deferred.resolve(token);
                        }, function (error) {
                            deferred.reject(error);
                        }
                    );

                    return deferred.promise.then(function () {
                        return $http(response.config);
                    });
                }
                else if (response.status === 0 && response.data === '') {
                    $window.location.reload();
                }
                else {
                    return $q.reject(response);
                }
            }
        }
}]);

app.config(['$httpProvider', function ($httpProvider) {
    /// <summary>
    /// Http interceptor used to validate ADFS is active prior perform the action.
    /// </summary>
    /// <param name="$httpProvider">Angular provider that provides functionality for http.</param>
    $httpProvider.interceptors.push('httpInterceptorVersion');
    $httpProvider.interceptors.push('httpInterceptorToken');
}]);