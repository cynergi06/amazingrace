USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Horse_Tbl') IS NOT NULL
BEGIN
	TRUNCATE TABLE dbo.Trn_Horse_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts the horses of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE 
	  @CurrentDate DATETIME = GETDATE()
	, @UpdateBy VARCHAR(255) = 'system.administrator'

INSERT INTO dbo.Trn_Horse_Tbl (
	  HorseName
	, Odds
	, IsActive
	, CreateDate
	, UpdateBy
	, UpdateDate
)
SELECT
	  HorseName = ''
	, Odds = 0
	, IsActive = 'N'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Abbervail Dream'
	, Odds = 1.1
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Back In A Flash'
	, Odds = 2.2
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Captain Canada'
	, Odds = 3.3
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Dennis the Menace'
	, Odds = 4.4
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Eye of the Storm'
	, Odds = 5.5
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Faraway Kingdom'
	, Odds = 6.6
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Gentleman Jazz'
	, Odds = 7.7
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Hawk of the Wind'
	, Odds = 8.8
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Ima Golden Champagn'
	, Odds = 9.9
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
UNION ALL
SELECT
	  HorseName = 'Joshua Mountain Miracle'
	, Odds = 10.10
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = @UpdateBy
	, UpdateDate = NULL
GO