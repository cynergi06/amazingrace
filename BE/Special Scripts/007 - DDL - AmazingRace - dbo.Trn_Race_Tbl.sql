USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Race_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Trn_Race_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Trn_Race_Tbl
*	Description		: Contains the list of races of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Trn_Race_Tbl (
	  RaceKey BIGINT NOT NULL IDENTITY(1, 1)
	
	, RaceName VARCHAR(255) NOT NULL
	, RaceDate DATETIME NOT NULL
	, RaceStatusKey BIGINT NOT NULL
	, IsActive CHAR(2) NOT NULL
	, CreateDate DATETIME NOT NULL
	, UpdateBy VARCHAR(255) NOT NULL
	, UpdateDate DATETIME

	, CONSTRAINT PK_RaceKey_Trn_Race_Tbl PRIMARY KEY CLUSTERED (RaceKey)
	, CONSTRAINT FK_RaceStatusKey_Trn_Race_Tbl FOREIGN KEY (RaceStatusKey)
		REFERENCES dbo.Bas_Race_Status_Tbl (RaceStatusKey)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX UI_INC_RaceName_Trn_Race_Tbl ON dbo.Trn_Race_Tbl (RaceName)
INCLUDE (RaceDate, RaceStatusKey, IsActive, CreateDate, UpdateBy, UpdateDate)
ON [INDEX]
GO

CREATE NONCLUSTERED INDEX IX_INC_IsActive_Trn_Race_Tbl ON dbo.Trn_Race_Tbl (IsActive)
INCLUDE (RaceName, RaceDate, RaceStatusKey, CreateDate, UpdateBy, UpdateDate)
ON [INDEX]
GO

ALTER TABLE dbo.Trn_Race_Tbl ADD CONSTRAINT DF_IsActive_Trn_Race_Tbl DEFAULT 'Y' FOR IsActive
GO

ALTER TABLE dbo.Trn_Race_Tbl ADD CONSTRAINT DF_CreateDate_Trn_Race_Tbl DEFAULT GETDATE() FOR CreateDate
GO