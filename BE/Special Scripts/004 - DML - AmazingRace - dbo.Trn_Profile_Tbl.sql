USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Profile_Tbl') IS NOT NULL
BEGIN
	TRUNCATE TABLE dbo.Trn_Profile_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts 1 inactive profile in the application
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE @CurrentDate DATETIME = GETDATE()

INSERT INTO dbo.Trn_Profile_Tbl (
	  FirstName
	, MiddleName
	, LastName
	, EnterpriseId
	, EnterpriseToken
	, IsActive
	, CreateDate
	, UpdateBy
	, UpdateDate
	, RoleKey)
SELECT
	  FirstName = ''
	, MiddleName = ''
	, LastName = ''
	, EnterpriseId = 'default.customer'
	, EnterpriseToken = ''
	, IsActive = 'N'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = NULL
	, RoleKey = 1
UNION ALL
SELECT
	  FirstName = ''
	, MiddleName = ''
	, LastName = ''
	, EnterpriseId = 'default.raceowner'
	, EnterpriseToken = ''
	, IsActive = 'N'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = NULL
	, RoleKey = 2
GO