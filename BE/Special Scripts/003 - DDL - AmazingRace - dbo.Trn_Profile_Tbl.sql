USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Profile_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Trn_Profile_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Trn_Profile_Tbl
*	Description		: Contains the list of profiles of the application
*					  i.e. Customer, Race Owner
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Trn_Profile_Tbl (
	  ProfileKey BIGINT NOT NULL IDENTITY(1, 1)
	
	, FirstName VARCHAR(100) NOT NULL
	, MiddleName VARCHAR(100)
	, LastName VARCHAR(100)
	, EnterpriseId VARCHAR(255) NOT NULL
	, EnterpriseToken VARCHAR(255) NOT NULL
	, IsActive CHAR(2) NOT NULL
	, CreateDate DATETIME NOT NULL
	, UpdateBy VARCHAR(255) NOT NULL
	, UpdateDate DATETIME

	, RoleKey INT NOT NULL

	, CONSTRAINT PK_ProfileKey_Trn_Profile_Tbl PRIMARY KEY CLUSTERED (ProfileKey)
	, CONSTRAINT FK_RoleKey_Trn_Profile_Tbl FOREIGN KEY (RoleKey)
		REFERENCES dbo.Bas_Role_Tbl (RoleKey)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX UI_INC_EnterpriseId_Trn_Profile_Tbl ON dbo.Trn_Profile_Tbl (EnterpriseId)
INCLUDE (FirstName, MiddleName, LastName, EnterpriseToken, IsActive, CreateDate, UpdateBy, UpdateDate, RoleKey)
ON [INDEX]
GO

CREATE NONCLUSTERED INDEX IX_INC_IsActive_Trn_Profile_Tbl ON dbo.Trn_Profile_Tbl (IsActive)
INCLUDE (FirstName, MiddleName, LastName, EnterpriseId, EnterpriseToken, CreateDate, UpdateBy, UpdateDate, RoleKey)
ON [INDEX]
GO

ALTER TABLE dbo.Trn_Profile_Tbl ADD CONSTRAINT DF_IsActive_Trn_Profile_Tbl DEFAULT 'Y' FOR IsActive
GO

ALTER TABLE dbo.Trn_Profile_Tbl ADD CONSTRAINT DF_CreateDate_Trn_Profile_Tbl DEFAULT GETDATE() FOR CreateDate
GO

ALTER TABLE dbo.Trn_Profile_Tbl ADD CONSTRAINT DF_UpdateBy_Trn_Profile_Tbl DEFAULT '' FOR UpdateBy
GO