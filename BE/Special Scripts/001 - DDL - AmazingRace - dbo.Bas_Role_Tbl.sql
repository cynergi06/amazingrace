USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Bas_Role_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Bas_Role_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Bas_Role_Tbl
*	Description		: Contains the list of roles of the application
*					  i.e. Customer, Race Owner
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Bas_Role_Tbl (
	  RoleKey INT NOT NULL IDENTITY(1, 1)
	
	, RoleName VARCHAR(100) NOT NULL
	, RoleDescription VARCHAR(255) NOT NULL
	, StartDate DATETIME NOT NULL
	, EndDate DATETIME NOT NULL

	, CONSTRAINT PK_RoleKey_Bas_Role_Tbl PRIMARY KEY CLUSTERED (RoleKey)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX UI_INC_RoleName_Bas_Role_Tbl ON dbo.Bas_Role_Tbl (RoleName)
INCLUDE (RoleDescription, StartDate, EndDate)
ON [INDEX]
GO

CREATE NONCLUSTERED INDEX IX_INC_StartDateEndDate_Bas_Role_Tbl ON dbo.Bas_Role_Tbl (StartDate, EndDate)
INCLUDE (RoleName, RoleDescription)
ON [INDEX]
GO

ALTER TABLE dbo.Bas_Role_Tbl ADD CONSTRAINT DF_RoleName_Bas_Role_Tbl DEFAULT '' FOR RoleName
GO

ALTER TABLE dbo.Bas_Role_Tbl ADD CONSTRAINT DF_RoleDescription_Bas_Role_Tbl DEFAULT '' FOR RoleDescription
GO

ALTER TABLE dbo.Bas_Role_Tbl ADD CONSTRAINT DF_StartDate_Bas_Role_Tbl DEFAULT GETDATE() FOR StartDate
GO

ALTER TABLE dbo.Bas_Role_Tbl ADD CONSTRAINT DF_EndDate_Bas_Role_Tbl DEFAULT '01/01/9999' FOR EndDate
GO