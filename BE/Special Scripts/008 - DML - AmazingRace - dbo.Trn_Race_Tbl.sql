USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Race_Tbl') IS NOT NULL
BEGIN
	DELETE FROM dbo.Trn_Race_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts the races of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE @CurrentDate DATETIME = GETDATE()

INSERT INTO dbo.Trn_Race_Tbl (
	  RaceName
	, RaceDate
	, RaceStatusKey
	, IsActive
	, CreateDate
	, UpdateBy
	, UpdateDate
)
SELECT
	  RaceName = ''
	, RaceDate = '01/01/1900'
	, RaceStatusKey = 1
	, IsActive = 'N'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = NULL
--------------- PENDING ---------------
UNION ALL
SELECT
	  RaceName = 'Race 1 - Pending'
	, RaceDate = @CurrentDate + 5
	, RaceStatusKey = 2
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = NULL
UNION ALL
SELECT
	  RaceName = 'Race 2 - Pending'
	, RaceDate = @CurrentDate + 10
	, RaceStatusKey = 2
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = NULL
--------------- IN-PROGRESS ---------------
UNION ALL
SELECT
	  RaceName = 'Race 3 - In-Progress'
	, RaceDate = @CurrentDate
	, RaceStatusKey = 3
	, IsActive = 'Y'
	, CreateDate = @CurrentDate
	, UpdateBy = 'system.administrator'
	, UpdateDate = @CurrentDate
--------------- COMPLETED ---------------
UNION ALL
SELECT
	  RaceName = 'Race 4 - Completed'
	, RaceDate = @CurrentDate - 1
	, RaceStatusKey = 4
	, IsActive = 'Y'
	, CreateDate = @CurrentDate - 1
	, UpdateBy = 'system.administrator'
	, UpdateDate = @CurrentDate - 1
GO