USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Race_Bet_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Trn_Race_Bet_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Trn_Race_Bet_Tbl
*	Description		: Contains the list of race's bets of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Trn_Race_Bet_Tbl (
	  RaceBetKey BIGINT NOT NULL IDENTITY(1, 1)

	, RaceKey BIGINT NOT NULL
	, ProfileKey BIGINT NOT NULL
	, HorseKey BIGINT NOT NULL
	, Amount DECIMAL(9,2) NOT NULL
	, IsActive CHAR(2) NOT NULL
	, CreateDate DATETIME NOT NULL
	, UpdateBy VARCHAR(255) NOT NULL
	, UpdateDate DATETIME

	, CONSTRAINT PK_RaceBetKey_Trn_Race_Bet_Tbl PRIMARY KEY CLUSTERED (RaceBetKey)
	
	, CONSTRAINT FK_ProfileKey_Trn_Race_Bet_Tbl FOREIGN KEY (ProfileKey)
		REFERENCES dbo.Trn_Profile_Tbl (ProfileKey)
		ON UPDATE CASCADE
		ON DELETE CASCADE
	, CONSTRAINT FK_RaceKey_Trn_Race_Bet_Tbl FOREIGN KEY (RaceKey)
		REFERENCES dbo.Trn_Race_Tbl (RaceKey)
		ON UPDATE CASCADE
		ON DELETE CASCADE
	, CONSTRAINT FK_HorseKey_Trn_Horse_Tbl FOREIGN KEY (HorseKey)
		REFERENCES dbo.Trn_Horse_Tbl (HorseKey)
		ON UPDATE CASCADE
		ON DELETE CASCADE
)
ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_INC_IsActive_Trn_Race_Bet_Tbl ON dbo.Trn_Race_Bet_Tbl (IsActive)
INCLUDE (RaceKey, ProfileKey, HorseKey, Amount, CreateDate, UpdateBy, UpdateDate)
ON [INDEX]
GO

ALTER TABLE dbo.Trn_Race_Bet_Tbl ADD CONSTRAINT DF_IsActive_Trn_Race_Bet_Tbl DEFAULT 'Y' FOR IsActive
GO

ALTER TABLE dbo.Trn_Race_Bet_Tbl ADD CONSTRAINT DF_CreateDate_Trn_Race_Bet_Tbl DEFAULT GETDATE() FOR CreateDate
GO