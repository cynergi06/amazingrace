USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Bas_Race_Status_Tbl') IS NOT NULL
BEGIN
	TRUNCATE TABLE dbo.Bas_Race_Status_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts the race status of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE 
	  @CurrentDate DATETIME = GETDATE()
	, @EndDate DATETIME = '01/01/9999'

INSERT INTO dbo.Bas_Race_Status_Tbl (
	  RaceStatus
	, StartDate
	, EndDate
)
SELECT
	  RaceStatus = ''
	, StartDate = @CurrentDate
	, EndDate = '01/01/1900'
UNION ALL
SELECT
	  RaceStatus = 'Pending'
	, StartDate = @CurrentDate
	, EndDate = @EndDate
UNION ALL
SELECT
	  RaceStatus = 'In-Progress'
	, StartDate = @CurrentDate
	, EndDate = @EndDate
UNION ALL
SELECT
	  RaceStatus = 'Completed'
	, StartDate = @CurrentDate
	, EndDate = @EndDate
GO