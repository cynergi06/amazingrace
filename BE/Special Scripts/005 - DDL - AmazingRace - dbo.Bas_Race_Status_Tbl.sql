USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Bas_Race_Status_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Bas_Race_Status_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Bas_Race_Status_Tbl
*	Description		: Contains the list of status for the races of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Bas_Race_Status_Tbl (
	  RaceStatusKey BIGINT NOT NULL IDENTITY(1, 1)

	, RaceStatus VARCHAR(255) NOT NULL
	, StartDate DATETIME NOT NULL
	, EndDate DATETIME NOT NULL

	, CONSTRAINT PK_RaceStatusKey_Bas_Race_Status_Tbl PRIMARY KEY CLUSTERED (RaceStatusKey)
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX UI_INC_RaceStatus_Bas_Race_Status_Tbl ON dbo.Bas_Race_Status_Tbl (RaceStatus)
INCLUDE (StartDate, EndDate)
ON [INDEX]
GO

CREATE NONCLUSTERED INDEX IX_INC_StartDateEndDate_Bas_Race_Status_Tbl ON dbo.Bas_Race_Status_Tbl (StartDate, EndDate)
INCLUDE (RaceStatus)
ON [INDEX]
GO

ALTER TABLE dbo.Bas_Race_Status_Tbl ADD CONSTRAINT DF_StartDate_Bas_Race_Status_Tbl DEFAULT GETDATE() FOR StartDate
GO

ALTER TABLE dbo.Bas_Race_Status_Tbl ADD CONSTRAINT DF_EndDate_Bas_Race_Status_Tbl DEFAULT '01/01/9999' FOR EndDate
GO