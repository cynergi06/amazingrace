USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Horse_Tbl') IS NOT NULL
BEGIN
	DROP TABLE dbo.Trn_Horse_Tbl
END
GO

/********************************************************************************
*	Object Name		: dbo.Trn_Horse_Tbl
*	Description		: Contains the list of horses of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
CREATE TABLE dbo.Trn_Horse_Tbl (
	  HorseKey BIGINT NOT NULL IDENTITY(1, 1)

	, HorseName VARCHAR(255) NOT NULL
	, Odds DECIMAL(4,2) NOT NULL
	, IsActive CHAR(2) NOT NULL
	, CreateDate DATETIME NOT NULL
	, UpdateBy VARCHAR(255) NOT NULL
	, UpdateDate DATETIME

	, CONSTRAINT PK_HorseKey_Trn_Horse_Tbl PRIMARY KEY CLUSTERED (HorseKey)
)
ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX IX_INC_IsActive_Trn_Horse_Tbl ON dbo.Trn_Horse_Tbl (IsActive)
INCLUDE (HorseName, Odds, CreateDate, UpdateBy, UpdateDate)
ON [INDEX]
GO

ALTER TABLE dbo.Trn_Horse_Tbl ADD CONSTRAINT DF_IsActive_Trn_Horse_Tbl DEFAULT 'Y' FOR IsActive
GO

ALTER TABLE dbo.Trn_Horse_Tbl ADD CONSTRAINT DF_CreateDate_Trn_Horse_Tbl DEFAULT GETDATE() FOR CreateDate
GO