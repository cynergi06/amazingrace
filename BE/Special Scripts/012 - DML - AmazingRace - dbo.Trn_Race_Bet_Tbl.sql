USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Trn_Race_Bet_Tbl') IS NOT NULL
BEGIN
	DELETE FROM dbo.Trn_Race_Bet_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts the race's bets of the application
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE @CurrentDate DATETIME = GETDATE()

INSERT INTO dbo.Trn_Race_Bet_Tbl (
	  RaceKey
	, ProfileKey
	, HorseKey
	, Amount
	, IsActive
	, CreateDate
	, UpdateBy
	, UpdateDate
)
----------- RACE KEY: 2 -----------
SELECT RaceKey = 2, ProfileKey = 3, HorseKey = 2, Amount = 100.50, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 3, HorseKey = 3, Amount = 100.60, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 3, HorseKey = 4, Amount = 100.70, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 2, ProfileKey = 4, HorseKey = 3, Amount = 100.80, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 4, HorseKey = 4, Amount = 100.90, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 4, HorseKey = 5, Amount = 200.00, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 2, ProfileKey = 5, HorseKey = 4, Amount = 200.10, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 5, HorseKey = 5, Amount = 200.20, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 2, ProfileKey = 5, HorseKey = 6, Amount = 200.30, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

----------- RACE KEY: 3 -----------
SELECT RaceKey = 3, ProfileKey = 4, HorseKey = 5, Amount = 200.40, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 4, HorseKey = 6, Amount = 200.50, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 4, HorseKey = 7, Amount = 200.60, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 3, ProfileKey = 5, HorseKey = 6, Amount = 200.70, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 5, HorseKey = 7, Amount = 200.80, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 5, HorseKey = 8, Amount = 200.90, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 3, ProfileKey = 6, HorseKey = 7, Amount = 300.00, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 6, HorseKey = 8, Amount = 300.10, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 6, HorseKey = 9, Amount = 300.20, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 3, ProfileKey = 7, HorseKey = 8, Amount = 300.30, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 7, HorseKey = 9, Amount = 300.40, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 3, ProfileKey = 7, HorseKey = 10, Amount = 300.50, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

----------- RACE KEY: 4 -----------
SELECT RaceKey = 4, ProfileKey = 3, HorseKey = 9, Amount = 300.60, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 3, HorseKey = 10, Amount = 300.70, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 3, HorseKey = 11, Amount = 300.80, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 4, ProfileKey = 4, HorseKey = 10, Amount = 300.90, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 4, HorseKey = 11, Amount = 400.00, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 4, HorseKey = 2, Amount = 400.10, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 4, ProfileKey = 5, HorseKey = 11, Amount = 400.20, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 5, HorseKey = 2, Amount = 400.30, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 5, HorseKey = 3, Amount = 400.40, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 4, ProfileKey = 6, HorseKey = 2, Amount = 400.50, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 6, HorseKey = 3, Amount = 400.60, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 6, HorseKey = 4, Amount = 400.70, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 4, ProfileKey = 7, HorseKey = 3, Amount = 400.80, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 7, HorseKey = 4, Amount = 400.90, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 4, ProfileKey = 7, HorseKey = 5, Amount = 500.00, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

----------- RACE KEY: 4 -----------
SELECT RaceKey = 5, ProfileKey = 3, HorseKey = 4, Amount = 500.10, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 5, ProfileKey = 3, HorseKey = 5, Amount = 500.20, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 5, ProfileKey = 3, HorseKey = 6, Amount = 500.30, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL

SELECT RaceKey = 5, ProfileKey = 4, HorseKey = 5, Amount = 500.40, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 5, ProfileKey = 4, HorseKey = 6, Amount = 500.50, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL UNION ALL
SELECT RaceKey = 5, ProfileKey = 4, HorseKey = 7, Amount = 500.60, IsActive = 'Y', CreateDate = @CurrentDate, UpdateBy = 'system.administrator', UpdateDate = NULL 
GO