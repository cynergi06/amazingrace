USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.Bas_Role_Tbl') IS NOT NULL
BEGIN
	TRUNCATE TABLE dbo.Bas_Role_Tbl
END
GO

/********************************************************************************
*	Object Name		: N/A
*	Description		: Inserts the list of roles of the application
*					  i.e. Customer, Race Owner
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*
********************************************************************************/
DECLARE @CurrentDate DATETIME = GETDATE()

INSERT INTO dbo.Bas_Role_Tbl (
	  RoleName
	, RoleDescription
	, StartDate
	, EndDate)
SELECT 
	  RoleName = 'Customer'
	, RoleDescription = 'Profile that can modify horses and bets on races.'
	, StartDate = @CurrentDate
	, EndDate = '01/01/9999' 
UNION ALL
SELECT 
	  RoleName = 'Race Owner'
	, RoleDescription = 'Profile that can modify horses, modify and bets on races.'
	, StartDate = @CurrentDate
	, EndDate = '01/01/9999'
GO