USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.HashEnterpriseToken_FN') IS NOT NULL
BEGIN
	DROP FUNCTION dbo.HashEnterpriseToken_FN
END
GO

/********************************************************************************
*	Object Name		: dbo.HashEnterpriseToken_FN
*	Description		: Encrypts the enterprise token of the profile
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*	VARCHAR			|Hashed enterprise token
********************************************************************************/
CREATE FUNCTION dbo.HashEnterpriseToken_FN (
	  @EnterpriseId VARCHAR(255)
	, @EnterpriseToken VARCHAR(255)
) RETURNS VARCHAR(MAX) AS
BEGIN
	DECLARE 
		  @ErrorMessage VARCHAR(255) = ''
		, @HashedEnterpriseToken VARCHAR(MAX) = ''

	IF @EnterpriseId = '' OR @EnterpriseToken = ''
	BEGIN
		SET @ErrorMessage = 'EnterpriseId or EnterpriseToken must not be null.'
		GOTO ABEND
	END

	SELECT @HashedEnterpriseToken = CONVERT(VARCHAR(MAX), HASHBYTES('SHA2_256', CONCAT(@EnterpriseId, @EnterpriseToken)), 1)

	RETURN @HashedEnterpriseToken

	ABEND:
		RETURN 'Invalid EnterpriseId and/or EnterpriseToken.'
END
GO