USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.LoadRacePerHorse_SP') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.LoadRacePerHorse_SP
END
GO

/********************************************************************************
*	Object Name		: dbo.LoadRacePerHorse_SP
*	Description		: The stored procedure that gets the list of races per horse
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*	Table			|List of races of the application
********************************************************************************/
CREATE PROCEDURE dbo.LoadRacePerHorse_SP
	  @RaceKey BIGINT = 0
	, @HorseKey BIGINT = 0
AS
BEGIN
	DECLARE @CurrentDate DATETIME = GETDATE()

	--1.) Get race bet details
	;WITH 
		GetRaceBetDetails_CTE AS (
			SELECT DISTINCT
				  rb.RaceKey
				, rb.HorseKey
			FROM dbo.Trn_Race_Bet_Tbl rb WITH (NOLOCK)
			WHERE
				rb.IsActive = 'Y'
				AND (@RaceKey = 0 OR rb.RaceKey = @RaceKey)
				AND (@HorseKey = 0 OR rb.HorseKey = @HorseKey)
		)

	--2.) Calculate Amount
	SELECT
		  src.RaceKey

		, r.RaceName
		, r.RaceDate
		, r.RaceStatusKey

		, rs.RaceStatus

		, src.HorseKey

		, h.HorseName
		, h.Odds

		, TotalAmount = CAST(ISNULL(amnt.TotalAmount, 0) AS DECIMAL(10,2))
		, TotalBets = CAST(ISNULL(amnt.TotalBets, 0) AS INT)
	FROM 
		GetRaceBetDetails_CTE src
		JOIN dbo.Trn_Race_Tbl r WITH (NOLOCK) ON src.RaceKey = r.RaceKey
			AND r.IsActive = 'Y'
		JOIN dbo.Bas_Race_Status_Tbl rs WITH (NOLOCK) ON r.RaceStatusKey = rs.RaceStatusKey
			AND rs.StartDate <= @CurrentDate
			AND rs.EndDate > @CurrentDate
		JOIN dbo.Trn_Horse_Tbl h WITH (NOLOCK) ON src.HorseKey = h.HorseKey
			AND h.IsActive = 'Y'
		OUTER APPLY (
			SELECT 
				  src.RaceKey
				, src.HorseKey
				, TotalAmount = SUM(src.Amount)
				, TotalBets = SUM(src.Bets)
			FROM (
				SELECT 
					  rb1.RaceKey
					, rb1.ProfileKey
					, rb1.HorseKey
					, Amount = CAST(SUM(rb1.Amount * h1.Odds) AS DECIMAL(10,2))
					, Bets = COUNT(rb1.ProfileKey)
				FROM 
					dbo.Trn_Race_Bet_Tbl rb1 WITH (NOLOCK)
					JOIN dbo.Trn_Horse_Tbl h1 WITH (NOLOCK) on rb1.HorseKey = h1.HorseKey
						AND h1.IsActive = 'Y'
				WHERE 
					rb1.IsActive = 'Y'
					AND rb1.RaceKey = src.RaceKey
					AND rb1.HorseKey = src.HorseKey
				GROUP BY
					  rb1.RaceKey
					, rb1.ProfileKey
					, rb1.HorseKey) src
			GROUP BY src.RaceKey, src.HorseKey
		) amnt
END
GO