USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.CreateProfile_SP') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.CreateProfile_SP
END
GO

/********************************************************************************
*	Object Name			: dbo.CreateProfile_SP
*	Description			: The stored procedure that creates the profile
*	Updates				: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value		|Description
*--------------------------------------------------------------------------------
*	@@SCOPE_IDENTITY	|Success
********************************************************************************/
CREATE PROCEDURE dbo.CreateProfile_SP
	  @FirstName VARCHAR(100)
	, @MiddleName VARCHAR(100)
	, @LastName VARCHAR(100)
	, @EnterpriseId VARCHAR(255)
	, @EnterpriseToken VARCHAR(255)
	, @UpdateBy VARCHAR(255)
	, @RoleKey INT
AS
BEGIN
	DECLARE @CurrentDate DATETIME = GETDATE()

	INSERT INTO dbo.Trn_Profile_Tbl (
		  FirstName
		, MiddleName
		, LastName
		, EnterpriseId
		, EnterpriseToken
		, IsActive
		, CreateDate
		, UpdateBy
		, UpdateDate
		, RoleKey)
	SELECT
		  FirstName = @FirstName
		, MiddleName = @MiddleName
		, LastName = @LastName
		, EnterpriseId = @EnterpriseId
		, EnterpriseToken = dbo.HashEnterpriseToken_FN(@EnterpriseId, @EnterpriseToken)
		, IsActive = 'Y'
		, CreateDate = @CurrentDate
		, UpdateBy = @UpdateBy
		, UpdateDate = NULL
		, RoleKey = @RoleKey

	SELECT ID = CAST(SCOPE_IDENTITY() AS BIGINT)
END
GO