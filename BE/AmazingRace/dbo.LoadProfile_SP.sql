USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.LoadProfile_SP') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.LoadProfile_SP
END
GO

/********************************************************************************
*	Object Name		: dbo.LoadProfile_SP
*	Description		: The stored procedure that gets the list of profiles
*	Updates			: 11-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*	0				|Success
*	-1				|Failed
********************************************************************************/
CREATE PROCEDURE dbo.LoadProfile_SP
	  @EnterpriseId VARCHAR(255)
	, @EnterpriseToken VARCHAR(500)
AS
BEGIN
	DECLARE @CurrentDate DATETIME = GETDATE()

	SELECT
		  prfl.ProfileKey
		, prfl.FirstName
		, prfl.MiddleName
		, prfl.LastName
		, prfl.EnterpriseId
		, prfl.IsActive
		, prfl.CreateDate
		, prfl.UpdateBy
		, prfl.UpdateDate

		, prfl.RoleKey
		, rol.RoleName
		, rol.RoleDescription
	FROM
		dbo.Trn_Profile_Tbl prfl WITH (NOLOCK)
		JOIN dbo.Bas_Role_Tbl rol WITH (NOLOCK) ON prfl.RoleKey = rol.RoleKey
			AND rol.StartDate <= @CurrentDate
			AND rol.EndDate > @CurrentDate
	WHERE
		prfl.IsActive = 'Y'
		AND prfl.EnterpriseId = @EnterpriseId
		AND prfl.EnterpriseToken = dbo.HashEnterpriseToken_FN(@EnterpriseId, @EnterpriseToken)
END
GO