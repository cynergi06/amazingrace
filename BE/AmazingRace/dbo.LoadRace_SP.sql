USE AmazingRace
GO

SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID('dbo.LoadRace_SP') IS NOT NULL
BEGIN
	DROP PROCEDURE dbo.LoadRace_SP
END
GO

/********************************************************************************
*	Object Name		: dbo.LoadRace_SP
*	Description		: The stored procedure that gets the list of races
*	Updates			: 15-Jan-2018 Created by Charles Evan Frederick Y. Yu (ChEFY)
*
*	Return Value	|Description
*--------------------------------------------------------------------------------
*	Table			|List of races of the application
********************************************************************************/
CREATE PROCEDURE dbo.LoadRace_SP
	  @RaceKey BIGINT = 0
	, @RaceName VARCHAR(255) = ''
	, @RaceStatusKey BIGINT = 0
AS
BEGIN
	DECLARE @CurrentDate DATETIME = GETDATE()

	SELECT
		  r.RaceKey
		, r.RaceName
		, r.RaceDate

		, r.RaceStatusKey
		, rs.RaceStatus

		, TotalAmount = CAST(ISNULL(rb.TotalAmount, 0) AS DECIMAL(10, 2))

		, r.IsActive
		, r.CreateDate
		, r.UpdateBy
		, r.UpdateDate
	FROM
		dbo.Trn_Race_Tbl r WITH (NOLOCK)
		JOIN dbo.Bas_Race_Status_Tbl rs WITH (NOLOCK) ON r.RaceStatusKey = rs.RaceStatusKey
			AND rs.StartDate <= @CurrentDate
			AND rs.EndDate > @CurrentDate
		OUTER APPLY (
			SELECT
				  srb.RaceKey
				, TotalAmount = SUM(srb.Amount)
			FROM dbo.Trn_Race_Bet_Tbl srb WITH (NOLOCK)
			WHERE 
				srb.IsActive = 'Y'
				AND srb.RaceKey = r.RaceKey
			GROUP BY srb.RaceKey
		) rb
	WHERE
		r.IsActive = 'Y'
		AND (@RaceKey = 0 OR r.RaceKey = @RaceKey)
		AND (@RaceName = '' OR r.RaceName LIKE '%' + @RaceName + '%')
		AND (@RaceStatusKey = 0 OR r.RaceStatusKey = @RaceStatusKey)
END
GO